package bondiland.actions;

import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class SetBigFlyer implements IAction {
  public var name(default, null): String = Obfu.raw("setBigFlyer");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var v: Bool = ctx.getBoolOr(Obfu.raw("v"), true);
    if (v) {
      Bondiland.IS_BIG_ENABLED.add(ctx.getGame());
    } else {
      Bondiland.IS_BIG_ENABLED.remove(ctx.getGame());
    }
    return false;
  }
}
