package bondiland.actions;

import hf.Hf;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class AddSupaItem implements IAction {
  public var name(default, null): String = Obfu.raw("addSupaItem");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var id: Int = ctx.getInt(Obfu.raw("id"));
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();
    hf.entity.supa.SupaItem.attach(game, id);
    return false;
  }
}
