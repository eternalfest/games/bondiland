package bondiland;

import bondiland.actions.AddSupaItem;
import bondiland.actions.SetBigFlyer;
import hf.entity.bad.flyer.Baleine;
import hf.entity.Physics;
import hf.Entity;
import hf.Hf;
import hf.mode.GameMode;
import merlin.IAction;
import etwin.ds.WeakSet;
import patchman.IPatch;
import patchman.Ref;

@:build(patchman.Build.di())
class Bondiland {
  private static var SIZE_FACTOR: Float = 2;
  private static var BIG_FLYER(default, never): WeakSet<Entity> = new WeakSet();
  public static var IS_BIG_ENABLED(default, never): WeakSet<GameMode> = new WeakSet();

  @:diExport
  public var setBigFlyer(default, never): IAction = new SetBigFlyer();
  @:diExport
  public var addSupaItem(default, never): IAction = new AddSupaItem();

  @:diExport
  public var attachBigBondissante(default, never): IPatch = Ref.auto(Baleine.attach)
  .after(function(hf: Hf, g: GameMode, x: Float, y: Float, res: Baleine): Void {
    if (IS_BIG_ENABLED.exists(g)) {
      res.scale(SIZE_FACTOR * 100);
      BIG_FLYER.add(res);
    }
  });

  @:diExport
  public var fixGameGetClose(default, never): IPatch = Ref.auto(GameMode.getClose)
  .replace(function(hf: Hf, self: GameMode, type: Int, x: Float, y: Float, radius: Float, fl_onGround: Bool): Array<Entity> {
    var result: Array<Entity> = new Array();
    var radius2: Float = Math.pow(radius, 2);
    var bigRadius: Float = radius * SIZE_FACTOR;
    var bigRadius2: Float = Math.pow(bigRadius, 2);
    for (entity in self.getList(type)) {
      var isBig: Bool = BIG_FLYER.exists(entity);
      var r: Float = isBig ? bigRadius : radius;
      var r2: Float = isBig ? bigRadius2 : radius2;
      var dist2 = Math.pow(entity.x - x, 2) + Math.pow(entity.y - y, 2);
      if (dist2 <= r2) {
        if (Math.sqrt(dist2) <= r) {
          if (!fl_onGround || fl_onGround && entity.y <= y + hf.Data.CASE_HEIGHT) {
            result.push(entity);
          }
        }
      }
    }
    return result;
  });

  @:diExport
  public var fixFlyerUpdate(default, never): IPatch = Ref.auto(Physics.update)
  .wrap(function(hf: Hf, self: Physics, next: Physics -> Void): Void {
    if (!BIG_FLYER.exists(self)) {
      // Regular entity
      return next(self);
    }
    // Big entity: update with fixed collisions.
    untyped {
      // super.update();
      hf.Animator.prototype.update.call(self);
    }
    if (!self.fl_physics) {
      return;
    }
    self.updateCoords();
    if (self.fl_wind) {
      if (self.fl_stable && self.game.fl_wind) {
        self.dx += self.game.windSpeed * hf.Timer.tmod;
      }
    }
    if (self.fl_stable) {
      if (self.dy != 0 || self.world.getCase({x: self.fcx, y: self.fcy}) != hf.Data.GROUND) {
        self.fl_stable = false;
      }
    }
    if (self.dx != 0 || self.dy != 0 || !self.fl_stable) {
      if (!self.fl_skipNextGravity && !self.fl_stable && self.fl_gravity) {
        var v4 = 1.0;
        if (hf.Timer.tmod >= 2) {
          v4 = 1.1;
        }
        if (self.dy < 0) {
          self.dy += self.gravityFactor * hf.Data.GRAVITY * hf.Timer.tmod * v4;
        } else {
          if (self.fallStart == null) {
            self.fallStart = self.y;
          }
          if (self.game.fl_aqua && !self.fl_strictGravity) {
            self.dy += 0.3 * self.fallFactor * hf.Data.FALL_SPEED * hf.Timer.tmod * v4;
          } else {
            self.dy += self.fallFactor * hf.Data.FALL_SPEED * hf.Timer.tmod * v4;
          }
        }
      }
      self.fl_skipNextGravity = false;
      self.prefix();
      var v5 = self.calcSteps(self.dx, self.dy);
      var v3 = 0;
      while (true) {
        if (!(!self.fl_stopStepping && v3 < v5.total)) break;
        var v6 = self.cx;
        var v7 = self.cy;
        var v8 = self.fcx;
        var v9 = self.fcy;
        var v10 = self.x;
        var v11 = self.y;
        self.oldX = self.x;
        self.oldY = self.y;
        self.x += v5.dx;
        self.y += v5.dy;
        self.updateCoords();
        if (self.fl_hitWall) {
          var f: Float = SIZE_FACTOR - 0.7;
          var v12 = false;
          if (self.dx > 0 && self.world.getCase({x: hf.Entity.x_rtc(v10 + f * hf.Data.CASE_WIDTH), y: hf.Entity.y_rtc(v11)}) > 0) {
            v12 = true;
          }
          if (self.dx < 0 && self.world.getCase({x: hf.Entity.x_rtc(v10 - f * hf.Data.CASE_WIDTH), y: hf.Entity.y_rtc(v11)}) > 0) {
            v12 = true;
          }
          if (v12) {
            self.x = v10;
            v5.dx = 0;
            self.updateCoords();
            self.onHitWall();
          }
        }
        if (self.fl_hitBorder && (self.x < hf.Data.BORDER_MARGIN || self.x >= hf.Data.GAME_WIDTH - hf.Data.BORDER_MARGIN) || self.fl_hitWall && self.world.getCase({x: self.cx, y: hf.Entity.y_rtc(v11)}) > 0) {
          self.x = v10;
          v5.dx = 0;
          self.updateCoords();
          self.onHitWall();
        }
        if (self.fl_hitWall && v5.dy > 0 && !self.fl_kill) {
          if (self.world.getCase(hf.Entity.rtc(v10, v11 + Math.floor(hf.Data.CASE_HEIGHT / 2))) != hf.Data.WALL && self.world.getCase({x: self.fcx, y: self.fcy}) == hf.Data.WALL) {
            self.x = v10;
            v5.dx = 0;
            self.updateCoords();
            self.onHitWall();
          }
        }
        if (self.fl_hitGround && v5.dy >= 0) {
          if (self.world.getCase(hf.Entity.rtc(v10, v11 + Math.floor(hf.Data.CASE_HEIGHT / 2))) != hf.Data.GROUND && self.world.getCase({x: self.fcx, y: self.fcy}) == hf.Data.GROUND) {
            if (self.world.checkFlag({x: self.fcx, y: self.fcy}, hf.Data.IA_TILE)) {
              if (self.fl_skipNextGround) {
                self.fl_skipNextGround = false;
              } else {
                v5.dy = 0;
                self.onHitGround(self.y - self.fallStart);
                self.fallStart = null;
                self.updateCoords();
              }
            }
          }
        }
        if (self.fl_hitCeil && v5.dy <= 0) {
          var f: Float = SIZE_FACTOR - 0.3;
          if (self.world.getCase(hf.Entity.rtc(v10, v11 - Math.floor(f * hf.Data.CASE_HEIGHT))) <= 0 && self.world.getCase(hf.Entity.rtc(self.x, self.y - Math.floor(f * hf.Data.CASE_HEIGHT))) > 0) {
            v5.dy = 0;
            self.onHitCeil();
            self.updateCoords();
          }
        }
        if (v6 != self.cx || v7 != self.cy) {
          var v13 = false;
          if (self.fl_hitGround && v7 < self.cy) {
            if (self.needsPatch()) {
              if (self.world.getCase({x: v6, y: v7}) <= 0 && self.dy > 0 && self.world.getCase({x: self.cx, y: self.cy}) > 0 && self.cy < hf.Data.LEVEL_HEIGHT) {
                self.x = hf.Entity.x_ctr(v6);
                self.y = hf.Entity.y_ctr(v7);
                v5.dy = 0;
                self.updateCoords();
                self.onHitGround(self.y - self.fallStart);
                v13 = true;
              }
            }
          }
          if (self.fl_hitGround && self.dy >= 0 && v6 != self.cx && v7 != self.cy) {
            if (self.world.getCase({x: v6, y: v7}) <= 0 && self.world.getCase({x: self.cx, y: self.cy}) == hf.Data.GROUND) {
              self.x = hf.Entity.x_ctr(v6);
              self.y = hf.Entity.y_ctr(v7);
              v5.dx = 0;
              self.updateCoords();
              self.onHitWall();
              v13 = true;
            }
          }
          if (!v13) {
            self.tRem(v6, v7);
            self.infix();
            self.updateCoords();
            self.tAdd(self.cx, self.cy);
          }
        }
        ++v3;
      }
    }
    self.fl_stopStepping = false;
    self.postfix();
    if (self.fl_friction) {
      if ((self.game.fl_ice || self.fl_slide) && self.fl_stable) {
        if (self.slideFriction == null) {
          self.dx *= self.game.sFriction;
        } else {
          self.dx *= Math.pow(self.slideFriction, hf.Timer.tmod);
        }
      } else {
        self.dx *= self.game.xFriction;
        self.dy *= self.game.yFriction;
      }
    }
    if (Math.abs(self.dx) <= 0.2) {
      self.dx = 0.0;
    }
    if (Math.abs(self.dy) <= 0.2) {
      self.dy = 0.0;
    }
    if (self.y >= hf.Data.DEATH_LINE) {
      self.onDeathLine();
    }
  });

  public function new() {}
}
