package;

import bondiland.Bondiland;
import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import patchman.IPatch;
import patchman.Patchman;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
    bondiland: Bondiland,
    merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ): Void {
    Patchman.patchAll(patches, hf);
  }
}
